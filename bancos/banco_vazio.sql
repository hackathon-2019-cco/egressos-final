-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: 
-- ------------------------------------------------------
-- Server version	5.5.5-10.3.16-MariaDB-1:10.3.16+maria~stretch

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `sem-hybridauth`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `sem-hybridauth` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `sem-hybridauth`;

--
-- Table structure for table `altecoes_usuario`
--

DROP TABLE IF EXISTS `altecoes_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `altecoes_usuario` (
  `id_alteracao` int(11) NOT NULL AUTO_INCREMENT,
  `data_alteracao` varchar(255) NOT NULL,
  `tipo_alteracao` varchar(255) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `usuario_id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_alteracao`),
  KEY `fk_altecoes_usuario_usuario1_idx` (`usuario_id_usuario`),
  CONSTRAINT `fk_altecoes_usuario_usuario1` FOREIGN KEY (`usuario_id_usuario`) REFERENCES `usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `altecoes_usuario`
--

LOCK TABLES `altecoes_usuario` WRITE;
/*!40000 ALTER TABLE `altecoes_usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `altecoes_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `amigos`
--

DROP TABLE IF EXISTS `amigos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amigos` (
  `id_amigos` int(1) NOT NULL AUTO_INCREMENT,
  `id_usuario1` int(11) NOT NULL,
  `id_usuario2` int(11) NOT NULL,
  `id_status` int(11) NOT NULL,
  `data_aceitado` timestamp NULL DEFAULT NULL,
  `data_termino` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_amigos`),
  KEY `fk_amigos_id_usuario1` (`id_usuario1`),
  KEY `fk_amigos_id_usuario2` (`id_usuario2`),
  KEY `id_status` (`id_status`),
  CONSTRAINT `fk_amigos_id_usuario1` FOREIGN KEY (`id_usuario1`) REFERENCES `usuario` (`id_usuario`),
  CONSTRAINT `fk_amigos_id_usuario2` FOREIGN KEY (`id_usuario2`) REFERENCES `usuario` (`id_usuario`),
  CONSTRAINT `id_status` FOREIGN KEY (`id_status`) REFERENCES `status` (`id_status`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `amigos`
--

LOCK TABLES `amigos` WRITE;
/*!40000 ALTER TABLE `amigos` DISABLE KEYS */;
/*!40000 ALTER TABLE `amigos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campus`
--

DROP TABLE IF EXISTS `campus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campus` (
  `id_campus` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(80) NOT NULL,
  PRIMARY KEY (`id_campus`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campus`
--

LOCK TABLES `campus` WRITE;
/*!40000 ALTER TABLE `campus` DISABLE KEYS */;
INSERT INTO `campus` VALUES (1,'Araquari'),(2,'Abelardo Luz'),(3,'Blumenau'),(4,'Brusque'),(5,'Camburiú'),(6,'Concórdia'),(7,'Fraiburgo'),(8,'Ibirama'),(9,'Luzerna'),(10,'Rio do Sul'),(11,'Santa Rosa do Sul'),(12,'São Bento do Sul'),(13,'São Francisco do Sul'),(14,'Sombrio'),(15,'Videira'),(16,'Reitoria');
/*!40000 ALTER TABLE `campus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comentario`
--

DROP TABLE IF EXISTS `comentario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comentario` (
  `id_comentario` int(11) NOT NULL AUTO_INCREMENT,
  `comentario` text NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_post` int(11) NOT NULL,
  `data_comentario` varchar(255) NOT NULL,
  PRIMARY KEY (`id_comentario`),
  KEY `fk_foto_post_id_usuario` (`id_usuario`),
  KEY `fk_foto_post_id_post` (`id_post`),
  CONSTRAINT `fk_foto_post_id_post` FOREIGN KEY (`id_post`) REFERENCES `post` (`id_post`),
  CONSTRAINT `fk_foto_post_id_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comentario`
--

LOCK TABLES `comentario` WRITE;
/*!40000 ALTER TABLE `comentario` DISABLE KEYS */;
/*!40000 ALTER TABLE `comentario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `curso`
--

DROP TABLE IF EXISTS `curso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `curso` (
  `id_curso` int(11) NOT NULL AUTO_INCREMENT,
  `curso` varchar(80) NOT NULL,
  `id_campus` int(11) NOT NULL,
  PRIMARY KEY (`id_curso`),
  KEY `fk_curso_id_campus` (`id_campus`),
  CONSTRAINT `fk_curso_id_campus` FOREIGN KEY (`id_campus`) REFERENCES `campus` (`id_campus`)
) ENGINE=InnoDB AUTO_INCREMENT=140 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `curso`
--

LOCK TABLES `curso` WRITE;
/*!40000 ALTER TABLE `curso` DISABLE KEYS */;
INSERT INTO `curso` VALUES (1,'Curso Técnico em Agropecuária Concomitante ao Ensino Médio',2),(2,'Curso Técnico em Agropecuária Integrado ao Ensino Médio',2),(3,'Curso Técnico em Agropecuária Subsequente ao Ensino Médio',2),(4,'Licenciatura em Pedagogia',2),(5,'Técnico em Informática Integrado ao Ensino Médio',1),(6,'Técnico em Agropecuária Integrado ao Ensino Médio',1),(7,'Técnico em Química Integrado ao Ensino Médio',1),(8,'Tecnologia em Redes de Computadores',1),(9,'Técnico em Agrimensura Subsequente ao Ensino Médio',1),(10,'Mestrado em Produção e Sanidade Animal',1),(11,'Licenciatura em Química',1),(12,'Licenciatura em Ciências Agrícolas',1),(13,'Bacharelado em Agronomia',1),(14,'Bacharelado em Medicina Veterinária',1),(15,'Bacharelado em Sistemas de Informação',1),(16,'Especialização em Aquicultura',1),(17,'Especialização em Educação Matemática',1),(18,'Formação Inicial e Continuada – FIC',1),(19,'Especialização em Educação',2),(20,'Mestrado em Tecnologia e Ambiente',1),(21,'Técnico em Eletromecânica Integrado ao Ensino Médio',3),(22,'Técnico em Informática Integrado ao Ensino Médio',3),(23,'Técnico Subsequente em Eletromecânica',3),(24,'Técnico Subsequente em Mecânica',3),(25,'Curso Superior de Tecnologia em Análise e Desenvolvimento de Sistemas',3),(26,'Licenciatura em Pedagogia',3),(27,'Superior em Engenharia Elétrica',3),(28,'Educação com ênfase em Alfabetização',3),(29,'Educação com ênfase em Educação da Pequena Infância',3),(30,'Mestrado Profissional em Ensino',3),(31,'Técnico em Informática Integrado',4),(32,'Técnico em Química Integrado',4),(33,'Técnico em Cervejaria',4),(34,'Técnico em Informática',4),(35,'Técnico em Química',4),(36,'Licenciatura em Química',4),(37,'Tecnologia em Redes de Computadores',4),(38,'Técnico em Agropecuária Integrado ao Ensino Médio',5),(39,'Técnico em Controle Ambiental Integrado ao Ensino Médio',5),(40,'Técnico em Hospedagem Integrado ao Ensino Médio',5),(41,'Técnico em Informática Integrado ao Ensino Médio',5),(42,'Técnico em Defesa Civil Subsequente ao Ensino Médio',5),(43,'Técnico em Segurança do Trabalho Subsequente ao Ensino Médio',5),(44,'Técnico em Transações Imobiliárias Subsequente ao Ensino Médio',5),(45,'Bacharelado em Sistemas de Informação',5),(46,'Bacharelado em Licenciatura em Matemática',5),(47,'Licenciatura em Pedagogia',5),(48,'Tecnologia em Negócios Imobiliários',5),(49,'Tecnologia em Sistemas para Internet',5),(50,'Pós-Graduação em Gestão e Negócios',5),(51,'Pós-Graduação em Educação',5),(52,'Mestrado em Educação',5),(53,'Treinador e Instrutor de Cães-Guia',5),(54,'Técnico em Agropecuária',6),(55,'Técnico em Alimentos',6),(56,'Técnico em Informática para Internet',6),(57,'Agronomia',6),(58,'Bacharelado em Medicina Veterinária',6),(59,'Bacharelado em Engenharia de Alimentos',6),(60,'Licenciatura em Física',6),(61,'Licenciatura em Matemática',6),(62,'Pós-Graduação em Produção e Sanidade Animal',6),(63,'Pós-graduação em Educação Matemática',6),(64,'Técnico em Informática Integrado ao Ensino Médio',7),(65,'Técnico em Informática Subsequente ao Ensino Médio',7),(66,'Técnico em Segurança do Trabalho',7),(67,'Análise de Desenvolvimento de Sistemas',7),(68,'Pós-Graduação em Educação',7),(69,'Qualificação Profissional para Assistente de Recursos Humanos',7),(70,'Qualificação Profissional para Assistente em Administração',7),(71,'Qualificação Profissional para Teatro Musical',7),(72,'Qualificação Profissional para Auxiliar Educacional',7),(73,'Técnico em Vestuário integrado ao Ensino Médio',8),(74,'Técnico em Informática integrado ao Ensino Médio',8),(75,'Técnico em Administração integrado ao Ensino Médio',8),(76,'PROEJA – com Qualificação em Administração Comercial',8),(77,'Técnico em Administração integrado ao Ensino Médio',8),(78,'Técnico em Informática integrado ao Ensino Médio',8),(79,'Técnico em Vestuário integrado ao Ensino Médio',8),(80,'Tecnologia em Design de Moda',8),(81,'Pós-Graduação em Educação Interdisciplinar',8),(82,'Pós-Graduação Lato Sensu em Nível de Especialização em Moda',8),(83,'Ensino Médio Integrado em Automação Industrial',9),(84,'Ensino Médio Integrado em Mecânica',9),(85,'Ensino Médio Integrado em Segurança do Trabalho',9),(86,'Técnico Subsequente em Automação Industrial',9),(87,'Técnico Subsequente em Mecânica',9),(88,'Engenharia de Controle e Automação',9),(89,'Engenharia Mecânica',9),(90,'Técnico em Agroecologia Integrado ao Ensino Médio',10),(91,'Técnico em Agropecuária Integrado ao Ensino Médio',10),(92,'Técnico em Informática Integrado ao Ensino Médio',10),(93,'Bacharelado em Agronomia',10),(94,'Bacharelado em Ciência da Computação',10),(95,'Bacharelado em Engenharia Mecatrônica',10),(96,'Licenciatura em Física',10),(97,'Licenciatura em Matemática',10),(98,'Licenciatura em Pedagogia',10),(99,'Pós-graduação em Agronomia',10),(100,'Pós-graduação em Gestão de TI',10),(101,'Bacharelado em Engenharia Agronômica',11),(102,'Técnico em Agropecuária Integrado ao Ensino Médio',11),(103,'Técnico em Agropecuária Subsequente ao Ensino Médio',11),(104,'Engenharia de Computação',12),(105,'Engenharia de Controle e Automação',12),(106,'Técnico subsequente em Defesa Civil',12),(107,'Técnico subsequente em Logística',12),(108,'Técnico subsequente em Qualidade',12),(109,' Técnico Integrado ao Ensino Médio em Automação Industrial',12),(110,'Técnico Integrado ao Ensino Médio em Informática',12),(111,'Técnico Integrado ao Ensino Médio em Segurança do Trabalho',12),(112,'Bacharelado em Engenharia Elétrica',13),(113,'Tecnologia em Logística',13),(114,'Tecnologia em Redes de Computadores',13),(115,'PROEJA – FIC em Auxiliar Administrativo Integrado ao Ensino Médio',13),(116,'Técnico Integrado em Administração Instegrado ao Ensino Médio ',13),(117,'Técnico Integrado em Automação Industrial Integrado ao Ensino Médio',13),(118,'Técnico Integrado em Guia de Turismo Integrado ao Ensino Médio',13),(119,'Técnico Subsequente em Administração',13),(120,'Técnico Subsequente em Automação Industrial',13),(121,'Curso Técnico em Hospedagem Integrado ao Ensino Médio',14),(122,'Curso Técnico em Informática Integrado ao Ensino Médio',14),(123,'Licenciatura em Matemática',14),(124,'Tecnologia em Gestão de Turismo',14),(125,'Tecnologia em Redes de Computadores',14),(126,'Técnico em Agropecuária Integrado ao Ensino Médio',15),(127,'Técnico em Eletroeletrônica Integrado ao Ensino Médio',15),(128,'Técnico em Informática Integrado ao Ensino Médio',15),(129,'Curso Técnico em Agropecuária Subsequente ao Ensino médio',15),(130,'Curso Técnico em Eletrônica Subsequente ao Ensino médio',15),(131,'Curso Técnico em Eletrotécnica Subsequente ao Ensino médio',15),(132,'Curso Técnico em Segurança do Trabalho Subsequente ao Ensino médio',15),(133,'Bacharelado em Ciência da Computação',15),(134,'Bacharelado em Engenharia Elétrica',15),(135,'Bacharelado em Pedagogia',15),(136,'Pós-graduação em Desenvolvimento Rural e Agronegócio ',15),(137,'Pós-graduação em Educação',15),(138,'Especialização em Educação do Campo',2),(139,'Teste de versão',16);
/*!40000 ALTER TABLE `curso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `curtidas`
--

DROP TABLE IF EXISTS `curtidas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `curtidas` (
  `id_curtida` int(11) NOT NULL AUTO_INCREMENT,
  `id_post` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_status` int(11) NOT NULL,
  `data_like` timestamp NOT NULL DEFAULT current_timestamp(),
  `data_dislike` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_curtida`),
  KEY `fk_post_id_post` (`id_post`),
  KEY `fk_usuario_id_usuario` (`id_user`),
  CONSTRAINT `fk_usuario_id_usuario` FOREIGN KEY (`id_user`) REFERENCES `usuario` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=169 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `curtidas`
--

LOCK TABLES `curtidas` WRITE;
/*!40000 ALTER TABLE `curtidas` DISABLE KEYS */;
/*!40000 ALTER TABLE `curtidas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grupo`
--

DROP TABLE IF EXISTS `grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grupo` (
  `id_grupo` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(60) NOT NULL,
  `descricao` text NOT NULL,
  `ano` varchar(4) NOT NULL,
  `id_status` int(11) NOT NULL,
  `data_criacao` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_grupo`),
  KEY `fk_grupo_id_status` (`id_status`),
  CONSTRAINT `fk_grupo_id_status` FOREIGN KEY (`id_status`) REFERENCES `status` (`id_status`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grupo`
--

LOCK TABLES `grupo` WRITE;
/*!40000 ALTER TABLE `grupo` DISABLE KEYS */;
/*!40000 ALTER TABLE `grupo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `midia`
--

DROP TABLE IF EXISTS `midia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `midia` (
  `file_ID` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(400) NOT NULL,
  `file_size` varchar(10) NOT NULL,
  `data_insercao` varchar(255) DEFAULT NULL,
  `status_id_status` int(11) NOT NULL,
  PRIMARY KEY (`file_ID`),
  KEY `fk_midia_status1_idx` (`status_id_status`),
  CONSTRAINT `fk_midia_status1` FOREIGN KEY (`status_id_status`) REFERENCES `status` (`id_status`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=200 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `midia`
--

LOCK TABLES `midia` WRITE;
/*!40000 ALTER TABLE `midia` DISABLE KEYS */;
/*!40000 ALTER TABLE `midia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `midia_grupo`
--

DROP TABLE IF EXISTS `midia_grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `midia_grupo` (
  `midia_file_ID` int(11) NOT NULL,
  `grupo_id_grupo` int(11) NOT NULL,
  `data_alteracao` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`midia_file_ID`,`grupo_id_grupo`),
  KEY `fk_midia_has_grupo_grupo1_idx` (`grupo_id_grupo`),
  KEY `fk_midia_has_grupo_midia1_idx` (`midia_file_ID`),
  CONSTRAINT `fk_midia_has_grupo_grupo1` FOREIGN KEY (`grupo_id_grupo`) REFERENCES `grupo` (`id_grupo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_midia_has_grupo_midia1` FOREIGN KEY (`midia_file_ID`) REFERENCES `midia` (`file_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `midia_grupo`
--

LOCK TABLES `midia_grupo` WRITE;
/*!40000 ALTER TABLE `midia_grupo` DISABLE KEYS */;
/*!40000 ALTER TABLE `midia_grupo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `midia_post`
--

DROP TABLE IF EXISTS `midia_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `midia_post` (
  `midia_file_ID` int(11) NOT NULL,
  `post_id_post` int(11) NOT NULL,
  `data_alteracao` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`midia_file_ID`,`post_id_post`),
  KEY `fk_midia_has_post_post1_idx` (`post_id_post`),
  KEY `fk_midia_has_post_midia1_idx` (`midia_file_ID`),
  CONSTRAINT `fk_midia_has_post_midia1` FOREIGN KEY (`midia_file_ID`) REFERENCES `midia` (`file_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_midia_has_post_post1` FOREIGN KEY (`post_id_post`) REFERENCES `post` (`id_post`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `midia_post`
--

LOCK TABLES `midia_post` WRITE;
/*!40000 ALTER TABLE `midia_post` DISABLE KEYS */;
/*!40000 ALTER TABLE `midia_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `midia_usuario`
--

DROP TABLE IF EXISTS `midia_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `midia_usuario` (
  `midia_file_ID` int(11) NOT NULL,
  `usuario_id_usuario` int(11) NOT NULL,
  `data_alteracao` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`midia_file_ID`,`usuario_id_usuario`),
  KEY `fk_midia_has_usuario_usuario1_idx` (`usuario_id_usuario`),
  KEY `fk_midia_has_usuario_midia1_idx` (`midia_file_ID`),
  CONSTRAINT `fk_midia_has_usuario_midia1` FOREIGN KEY (`midia_file_ID`) REFERENCES `midia` (`file_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_midia_has_usuario_usuario1` FOREIGN KEY (`usuario_id_usuario`) REFERENCES `usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `midia_usuario`
--

LOCK TABLES `midia_usuario` WRITE;
/*!40000 ALTER TABLE `midia_usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `midia_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notificacao`
--

DROP TABLE IF EXISTS `notificacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notificacao` (
  `id_notificacao` int(11) NOT NULL AUTO_INCREMENT,
  `texto_notificacao` varchar(255) NOT NULL,
  `id_usuario_de` int(11) NOT NULL,
  `id_usuario_para` int(11) NOT NULL,
  `id_origem` int(11) NOT NULL,
  `tipo_notificacao_id_tipo` int(11) NOT NULL,
  `id_status` int(11) NOT NULL,
  `data_enviado` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_notificacao`),
  KEY `fk_notificacao_tipo_notificacao1_idx` (`tipo_notificacao_id_tipo`),
  KEY `id_usuario_de` (`id_usuario_de`),
  KEY `id_usuario_para` (`id_usuario_para`),
  KEY `id_status` (`id_status`),
  CONSTRAINT `fk_notificacao_tipo_notificacao1` FOREIGN KEY (`tipo_notificacao_id_tipo`) REFERENCES `tipo_notificacao` (`id_tipo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_status_id_status_notificacao` FOREIGN KEY (`id_status`) REFERENCES `status` (`id_status`),
  CONSTRAINT `fk_usuario_usuario_de_notificacao` FOREIGN KEY (`id_usuario_de`) REFERENCES `usuario` (`id_usuario`),
  CONSTRAINT `fk_usuario_usuario_para_notificacao` FOREIGN KEY (`id_usuario_para`) REFERENCES `usuario` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notificacao`
--

LOCK TABLES `notificacao` WRITE;
/*!40000 ALTER TABLE `notificacao` DISABLE KEYS */;
/*!40000 ALTER TABLE `notificacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post` (
  `id_post` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `data` timestamp NOT NULL DEFAULT current_timestamp(),
  `id_usuario` int(11) NOT NULL,
  `id_status` int(11) NOT NULL,
  `id_grupo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_post`),
  KEY `fk_post_id_usuario` (`id_usuario`),
  KEY `fk_post_id_status` (`id_status`),
  KEY `fk_post_grupo` (`id_grupo`),
  CONSTRAINT `post_ibfk_1` FOREIGN KEY (`id_status`) REFERENCES `status` (`id_status`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `post_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `post_ibfk_3` FOREIGN KEY (`id_grupo`) REFERENCES `grupo` (`id_grupo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS `status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status` (
  `id_status` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) NOT NULL,
  PRIMARY KEY (`id_status`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status`
--

LOCK TABLES `status` WRITE;
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
INSERT INTO `status` VALUES (1,'Ativo'),(2,'Verificando'),(3,'Excluido'),(7,'Aguardando Resposta'),(8,'Solicitação Aceita'),(9,'Solicitação Recusada'),(10,'Bloqueado');
/*!40000 ALTER TABLE `status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_notificacao`
--

DROP TABLE IF EXISTS `tipo_notificacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_notificacao` (
  `id_tipo` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) NOT NULL,
  PRIMARY KEY (`id_tipo`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_notificacao`
--

LOCK TABLES `tipo_notificacao` WRITE;
/*!40000 ALTER TABLE `tipo_notificacao` DISABLE KEYS */;
INSERT INTO `tipo_notificacao` VALUES (1,'Amizade'),(2,'Comentario');
/*!40000 ALTER TABLE `tipo_notificacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_usuario`
--

DROP TABLE IF EXISTS `tipo_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_usuario` (
  `id_tipo` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(255) NOT NULL,
  PRIMARY KEY (`id_tipo`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_usuario`
--

LOCK TABLES `tipo_usuario` WRITE;
/*!40000 ALTER TABLE `tipo_usuario` DISABLE KEYS */;
INSERT INTO `tipo_usuario` VALUES (1,'Usuario'),(2,'Administrador');
/*!40000 ALTER TABLE `tipo_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `turma`
--

DROP TABLE IF EXISTS `turma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `turma` (
  `id_turma` int(11) NOT NULL AUTO_INCREMENT,
  `turma` varchar(20) NOT NULL,
  `id_curso` int(11) NOT NULL,
  PRIMARY KEY (`id_turma`),
  KEY `id_curso_fk` (`id_curso`),
  CONSTRAINT `turma_ibfk_1` FOREIGN KEY (`id_curso`) REFERENCES `curso` (`id_curso`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `turma`
--

LOCK TABLES `turma` WRITE;
/*!40000 ALTER TABLE `turma` DISABLE KEYS */;
INSERT INTO `turma` VALUES (3,'BSI 1',15),(4,'MEDVET 1',14),(5,'LICA 1',12),(6,'LIQUI 1',11),(7,'AGRON 1',13),(8,'ESPAQUI 1',16),(9,'EEM 1',17),(10,'FIC 1',18),(11,'MPSA 1',10),(12,'REDES 1',8),(13,'AGRI',9),(14,'AGRO 1',6),(15,'AGRO 2',6),(16,'AGRO 3',6),(17,'INFO 1',5),(18,'INFO 2',5),(19,'INFO 3',5),(20,'QUIMI 1',7),(21,'Versão de testes',139);
/*!40000 ALTER TABLE `turma` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(60) NOT NULL,
  `ultimo_nome` varchar(255) NOT NULL,
  `nome_completo` varchar(255) NOT NULL,
  `senha` varchar(60) NOT NULL,
  `email` varchar(60) NOT NULL,
  `id_status` int(1) NOT NULL,
  `descricao` text NOT NULL,
  `id_turma` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `id_grupo` int(11) NOT NULL,
  `ano_egresso` varchar(255) NOT NULL,
  `facebook` varchar(255) DEFAULT 'vazio',
  `linkedin` varchar(255) DEFAULT 'vazio',
  `trabalho_atual` varchar(255) NOT NULL DEFAULT 'vazio',
  `formacao_academica` varchar(255) NOT NULL DEFAULT 'vazio',
  `oauth` varchar(30) DEFAULT NULL,
  `pergunta` varchar(255) NOT NULL,
  `resposta` varchar(60) NOT NULL,
  `cod_rec_senha` varchar(60) NOT NULL,
  `data_criacao` timestamp NOT NULL DEFAULT current_timestamp(),
  `id_data_alteracao` int(11) NOT NULL,
  `id_tipo_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_usuario`),
  KEY `fk_usuario_id_status` (`id_status`),
  KEY `fk_usuario_id_turma` (`id_turma`) USING BTREE,
  KEY `fk_usuario_id_grupo` (`id_grupo`),
  KEY `fk_usuario_tipo_usuario1_idx` (`id_tipo_usuario`),
  CONSTRAINT `fk_usuario_tipo_usuario1` FOREIGN KEY (`id_tipo_usuario`) REFERENCES `tipo_usuario` (`id_tipo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`id_status`) REFERENCES `status` (`id_status`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `usuario_ibfk_2` FOREIGN KEY (`id_turma`) REFERENCES `turma` (`id_turma`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `usuario_ibfk_5` FOREIGN KEY (`id_grupo`) REFERENCES `grupo` (`id_grupo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-08 13:11:37
